import { DestinosViajesState } from './models/destinos-viajes-state.model';
//redux init

export interface AppState {
  destinos: DestinosViajesState;
}
